INTRODUCTION
------------

This module provides the social wall from wallsio.


INSTALLATION
------------

Enable this module from extend list page /admin/modules.


CONFIGURATION
-------------

1. Login to your walls.io account using of this link https://app.walls.io/login.
2. Once logged in connect with your social media. Ex: Facebook, Instagram, Youtube, Twitter, Pinterest, Reddit, Linkedin etc.
3. Click on “Embed & Display” in the main navigation.
4. Select JavaScript.
5. Customize your widget and copy the code.
    - It support many layout, click "show advanced settings" for layout selction. https://walls.io/features/social-wall-design
    - We can select height and width, lazyload, background and more.
6. Embed the code on configuration path (/admin/config/wallsio/settings).
