<?php

namespace Drupal\wallsio_social_wall\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * This config form is used to get wallsio embed script code.
 */
class WallsioAdminSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'wallsio_social_wall';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'wallsio_social_wall.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $config = $this->config('wallsio_social_wall.settings');
    $form['embed_script'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Embed Script'),
      '#default_value' => $config->get('wallsio_embed_script'),
      '#required' => TRUE,
      '#description' => $this->t('Please create Walls IO Embed Script <a href=":url">Here</a> and paste in the above field.', [':url' => 'https://walls.io/']),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('wallsio_social_wall.settings')
      ->set('wallsio_embed_script', $form_state->getValue('embed_script'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
