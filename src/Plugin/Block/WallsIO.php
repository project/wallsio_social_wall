<?php

namespace Drupal\wallsio_social_wall\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a walls IO Social Wall Block.
 *
 * @Block(
 *   id = "wallsio_social_wall",
 *   admin_label = @Translation("Walls IO Social Wall"),
 * )
 */
class WallsIO extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $config = \Drupal::config('wallsio_social_wall.settings');
    $embed = $config->get('wallsio_embed_script');

    return [
      '#theme' => 'embed_script',
      '#embed' => $embed,
    ];
  }

}
